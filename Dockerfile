FROM registry.gitlab.com/fdroid/ci-images-base:20180111
MAINTAINER team@f-droid.org

RUN apt-get update && apt-get -qy dist-upgrade && apt-get install -qy --no-install-recommends \
		gcc \
		git \
		gnupg \
		libjpeg-dev \
		libffi-dev \
		libssl-dev \
		make \
		python3-babel \
		python3-dev \
		python3-defusedxml \
		python3-pip \
		python3-ruamel.yaml \
		python3-setuptools \
		python3-venv \
		rsync \
		ruby \
		zlib1g-dev \
		`apt-cache depends fdroidserver | grep -Fv -e java -e jdk -e '<' | awk '/Depends:/{print$2}'` \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

COPY test /
