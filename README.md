# F-Droid Server CI image

This Docker image is used in
[fdroidserver](https://gitlab.com/fdroid/fdroidserver)'s continuous
integration via Gitlab.  It is built on top of our
[ci-images-base](https://gitlab.com/fdroid/ci-images-base) Docker
image.  It includes stuff that only the server tests need, like Python
linters.
